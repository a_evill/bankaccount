package ru.aev.bankaccount;

class DecreaseMoney extends Thread{
    private final BankAccount bankAccount;
    DecreaseMoney(BankAccount bankAccount){
        this.bankAccount = bankAccount;
    }

    public void run() {
        synchronized (bankAccount) {
            for (int i = 0; i <= 1_000_000; i++) {
                if(bankAccount.getBalance() > 100) {
                    bankAccount.setBalance(bankAccount.getBalance() - 100);
                    System.out.println("Сняли с аккаунта: " + 100 + " руб");
                }
                /*else{
                    bankAccount.notifyAll();
                } */
            }
            System.out.println("Осталось: " + bankAccount.getBalance());
        }
    }
}
