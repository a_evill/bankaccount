package ru.aev.bankaccount;

class BankAccount{
    private int balance;

    BankAccount(){
        this.balance = 0;
    }

    int getBalance() {
        return balance;
    }

   void setBalance(int balance) {
        this.balance = balance;
    }
}
