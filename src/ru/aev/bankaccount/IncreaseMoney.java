package ru.aev.bankaccount;

class IncreaseMoney extends Thread {
    private final BankAccount bankAccount;
    IncreaseMoney(BankAccount bankAccount){ this.bankAccount = bankAccount; }

    public void run() {
        synchronized (bankAccount) {
            for (int i = 1; i <= 5; i++) {
                int random = (int) (Math.random() * 20000);
                    bankAccount.setBalance(bankAccount.getBalance() + random);
                    System.out.println("На аккаунт зачисленно: " + random + " руб");
           /*         try {
                        bankAccount.wait();
                    } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            */
            }
        }
    }

}
